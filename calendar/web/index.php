<?php

date_default_timezone_set('Europe/Berlin');

require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../../common/config/bootstrap.php';
require __DIR__ . '/../config/bootstrap.php';

$config = yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/../../common/config/main.php',
    require __DIR__ . '/../../common/config/main-local.php',
    require __DIR__ . '/../config/main.php',
    require __DIR__ . '/../config/main-local.php'
    );

use calendar\backends\Authentication;

$app = new yii\web\Application($config);
$app->init();

$calendarBackend = new Sabre\CalDAV\Backend\PDO($app->db->getMasterPdo());
$principalBackend = new Sabre\DAVACL\PrincipalBackend\PDO($app->db->getMasterPdo());
$authPlugin = new \Sabre\DAV\Auth\Plugin(new Authentication);

// Directory structure
$tree = [
    new Sabre\CalDAV\Principal\Collection($principalBackend),
    new Sabre\CalDAV\CalendarRoot($principalBackend, $calendarBackend),
];

//Mapping PHP errors to exceptions
function exception_error_handler($errno, $errstr, $errfile, $errline) {
    Yii::error($errno);
    Yii::error($errstr);
    Yii::error($errfile);
    Yii::error($errline);
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}
set_error_handler("exception_error_handler");

$server = new \Sabre\DAV\Server($tree);
$server->setBaseUri('/');
$server->addPlugin($authPlugin);

$server->addPlugin(new \Sabre\DAVACL\Plugin());
$server->addPlugin(new \Sabre\CalDAV\Plugin());

/* Calendar scheduling support */
$server->addPlugin(
    new Sabre\CalDAV\Schedule\Plugin()
);

// this is fun, try to open that action in a plain web browser
//$server->addPlugin(new \Sabre\DAV\Browser\Plugin());

$server->exec();