<?php
namespace calendar\backends;

use common\models\User;
use Sabre\DAV\Auth\Backend\AbstractBasic;

class Authentication extends AbstractBasic
{
    protected $_identity;
    /**
     * Authentication Realm.
     *
     * The realm is often displayed by browser clients when showing the
     * authentication dialog.
     *
     * @var string
     */
    protected $realm = 'sabre/dav';
 
    protected function validateUserPass($username, $password)
    {
        if ($this->_identity === null && ($this->_identity = User::find()->active()->byUsername($username)->one())===null) {
            \Yii::error('User '.$username.' not found.', __METHOD__);
            return false;
        }
        if (!$this->_identity->validatePassword($password) || $this->_identity->status<User::STATUS_ACTIVE) {
            \Yii::error('User '.$username.' not activ or wrong password.', __METHOD__);
            return false;
        }
        return \Yii::$app->user->login($this->_identity);
    }
}