/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addressbookchanges`
--

DROP TABLE IF EXISTS `addressbookchanges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addressbookchanges` (
                                      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                      `uri` varbinary(200) NOT NULL,
                                      `synctoken` int(11) unsigned NOT NULL,
                                      `addressbookid` int(11) unsigned NOT NULL,
                                      `operation` tinyint(1) NOT NULL,
                                      PRIMARY KEY (`id`),
                                      KEY `addressbookid_synctoken` (`addressbookid`,`synctoken`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `addressbooks`
--

DROP TABLE IF EXISTS `addressbooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addressbooks` (
                                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                `principaluri` varbinary(255) DEFAULT NULL,
                                `displayname` varchar(255) DEFAULT NULL,
                                `uri` varbinary(200) DEFAULT NULL,
                                `description` text,
                                `synctoken` int(11) unsigned NOT NULL DEFAULT '1',
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `principaluri` (`principaluri`(100),`uri`(100))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appointments`
--

DROP TABLE IF EXISTS `appointments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appointments` (
                                `id` int(11) NOT NULL AUTO_INCREMENT,
                                `title` varchar(255) NOT NULL,
                                `description` text,
                                `beginDate` date NOT NULL,
                                `endDate` date NOT NULL,
                                `beginTime` time DEFAULT NULL,
                                `endTime` time DEFAULT NULL,
                                `repeatInterval` varchar(255) DEFAULT NULL,
                                `categories_id` int(11) NOT NULL,
                                PRIMARY KEY (`id`),
                                KEY `idx-appointments-categories_id` (`categories_id`),
                                CONSTRAINT `fk-appointments-categories_id` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appointments_groups`
--

DROP TABLE IF EXISTS `appointments_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appointments_groups` (
                                       `appointments_id` int(11) NOT NULL,
                                       `groups_id` int(11) NOT NULL,
                                       PRIMARY KEY (`appointments_id`,`groups_id`),
                                       KEY `idx-appointments_groups-appointments_id` (`appointments_id`),
                                       KEY `idx-appointments_groups-groups_id` (`groups_id`),
                                       CONSTRAINT `fk-appointments_groups-appointments_id` FOREIGN KEY (`appointments_id`) REFERENCES `appointments` (`id`) ON DELETE CASCADE,
                                       CONSTRAINT `fk-appointments_groups-groups_id` FOREIGN KEY (`groups_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `calendarchanges`
--

DROP TABLE IF EXISTS `calendarchanges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendarchanges` (
                                   `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                   `uri` varbinary(200) NOT NULL,
                                   `synctoken` int(11) unsigned NOT NULL,
                                   `calendarid` int(11) unsigned NOT NULL,
                                   `operation` tinyint(1) NOT NULL,
                                   PRIMARY KEY (`id`),
                                   KEY `calendarid_synctoken` (`calendarid`,`synctoken`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `calendarinstances`
--

DROP TABLE IF EXISTS `calendarinstances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendarinstances` (
                                     `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                     `calendarid` int(10) unsigned NOT NULL,
                                     `principaluri` varbinary(100) DEFAULT NULL,
                                     `access` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = owner, 2 = read, 3 = readwrite',
                                     `displayname` varchar(100) DEFAULT NULL,
                                     `uri` varbinary(200) DEFAULT NULL,
                                     `description` text,
                                     `calendarorder` int(11) unsigned NOT NULL DEFAULT '0',
                                     `calendarcolor` varbinary(10) DEFAULT NULL,
                                     `timezone` text,
                                     `transparent` tinyint(1) NOT NULL DEFAULT '0',
                                     `share_href` varbinary(100) DEFAULT NULL,
                                     `share_displayname` varchar(100) DEFAULT NULL,
                                     `share_invitestatus` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1 = noresponse, 2 = accepted, 3 = declined, 4 = invalid',
                                     PRIMARY KEY (`id`),
                                     UNIQUE KEY `principaluri` (`principaluri`,`uri`),
                                     UNIQUE KEY `calendarid` (`calendarid`,`principaluri`),
                                     UNIQUE KEY `calendarid_2` (`calendarid`,`share_href`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `calendarobjects`
--

DROP TABLE IF EXISTS `calendarobjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendarobjects` (
                                   `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                   `calendardata` mediumblob,
                                   `uri` varbinary(200) DEFAULT NULL,
                                   `calendarid` int(10) unsigned NOT NULL,
                                   `lastmodified` int(11) unsigned DEFAULT NULL,
                                   `etag` varbinary(32) DEFAULT NULL,
                                   `size` int(11) unsigned NOT NULL,
                                   `componenttype` varbinary(8) DEFAULT NULL,
                                   `firstoccurence` int(11) unsigned DEFAULT NULL,
                                   `lastoccurence` int(11) unsigned DEFAULT NULL,
                                   `uid` varbinary(200) DEFAULT NULL,
                                   PRIMARY KEY (`id`),
                                   UNIQUE KEY `calendarid` (`calendarid`,`uri`),
                                   KEY `calendarid_time` (`calendarid`,`firstoccurence`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `calendars`
--

DROP TABLE IF EXISTS `calendars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendars` (
                             `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                             `synctoken` int(10) unsigned NOT NULL DEFAULT '1',
                             `components` varbinary(21) DEFAULT NULL,
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `calendarsubscriptions`
--

DROP TABLE IF EXISTS `calendarsubscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendarsubscriptions` (
                                         `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                         `uri` varbinary(200) NOT NULL,
                                         `principaluri` varbinary(100) NOT NULL,
                                         `source` text,
                                         `displayname` varchar(100) DEFAULT NULL,
                                         `refreshrate` varchar(10) DEFAULT NULL,
                                         `calendarorder` int(11) unsigned NOT NULL DEFAULT '0',
                                         `calendarcolor` varbinary(10) DEFAULT NULL,
                                         `striptodos` tinyint(1) DEFAULT NULL,
                                         `stripalarms` tinyint(1) DEFAULT NULL,
                                         `stripattachments` tinyint(1) DEFAULT NULL,
                                         `lastmodified` int(11) unsigned DEFAULT NULL,
                                         PRIMARY KEY (`id`),
                                         UNIQUE KEY `principaluri` (`principaluri`,`uri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cards`
--

DROP TABLE IF EXISTS `cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards` (
                         `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                         `addressbookid` int(11) unsigned NOT NULL,
                         `carddata` mediumblob,
                         `uri` varbinary(200) DEFAULT NULL,
                         `lastmodified` int(11) unsigned DEFAULT NULL,
                         `etag` varbinary(32) DEFAULT NULL,
                         `size` int(11) unsigned NOT NULL,
                         PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `category` varchar(256) NOT NULL,
                              `description` varchar(255) DEFAULT NULL,
                              `html_color_code` varchar(7) DEFAULT NULL,
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categories_users`
--

DROP TABLE IF EXISTS `categories_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories_users` (
                                    `categories_id` int(11) NOT NULL,
                                    `users_id` int(11) NOT NULL,
                                    PRIMARY KEY (`categories_id`,`users_id`),
                                    KEY `idx-categories_users-categories_id` (`categories_id`),
                                    KEY `idx-categories_users-users_id` (`users_id`),
                                    CONSTRAINT `fk-categories_users-categories_id` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
                                    CONSTRAINT `fk-categories_users-users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `groupmembers`
--

DROP TABLE IF EXISTS `groupmembers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groupmembers` (
                                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                `principal_id` int(10) unsigned NOT NULL,
                                `member_id` int(10) unsigned NOT NULL,
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `principal_id` (`principal_id`,`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `group` varchar(256) NOT NULL,
                          `description` varchar(255) DEFAULT NULL,
                          `html_color_code` varchar(7) DEFAULT NULL,
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `groups_users`
--

DROP TABLE IF EXISTS `groups_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups_users` (
                                `groups_id` int(11) NOT NULL,
                                `users_id` int(11) NOT NULL,
                                PRIMARY KEY (`groups_id`,`users_id`),
                                KEY `idx-groups_users-groups_id` (`groups_id`),
                                KEY `idx-groups_users-users_id` (`users_id`),
                                CONSTRAINT `fk-groups_users-groups_id` FOREIGN KEY (`groups_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE,
                                CONSTRAINT `fk-groups_users-users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `locks`
--

DROP TABLE IF EXISTS `locks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locks` (
                         `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                         `owner` varchar(100) DEFAULT NULL,
                         `timeout` int(10) unsigned DEFAULT NULL,
                         `created` int(11) DEFAULT NULL,
                         `token` varbinary(100) DEFAULT NULL,
                         `scope` tinyint(4) DEFAULT NULL,
                         `depth` tinyint(4) DEFAULT NULL,
                         `uri` varbinary(1000) DEFAULT NULL,
                         PRIMARY KEY (`id`),
                         KEY `token` (`token`),
                         KEY `uri` (`uri`(100))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
                             `version` varchar(180) NOT NULL,
                             `apply_time` int(11) DEFAULT NULL,
                             PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `principals`
--

DROP TABLE IF EXISTS `principals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `principals` (
                              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                              `uri` varbinary(200) NOT NULL,
                              `email` varbinary(80) DEFAULT NULL,
                              `displayname` varchar(80) DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              UNIQUE KEY `uri` (`uri`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `propertystorage`
--

DROP TABLE IF EXISTS `propertystorage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `propertystorage` (
                                   `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                   `path` varbinary(1024) NOT NULL,
                                   `name` varbinary(100) NOT NULL,
                                   `valuetype` int(10) unsigned DEFAULT NULL,
                                   `value` mediumblob,
                                   PRIMARY KEY (`id`),
                                   UNIQUE KEY `path_property` (`path`(600),`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schedulingobjects`
--

DROP TABLE IF EXISTS `schedulingobjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedulingobjects` (
                                     `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                     `principaluri` varbinary(255) DEFAULT NULL,
                                     `calendardata` mediumblob,
                                     `uri` varbinary(200) DEFAULT NULL,
                                     `lastmodified` int(11) unsigned DEFAULT NULL,
                                     `etag` varbinary(32) DEFAULT NULL,
                                     `size` int(11) unsigned NOT NULL,
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
                         `id` int(11) NOT NULL AUTO_INCREMENT,
                         `username` varchar(255) NOT NULL,
                         `auth_key` varchar(32),
                         `password_hash` varchar(255) NOT NULL,
                         `password_reset_token` varchar(255) DEFAULT NULL,
                         `email` varchar(255) NOT NULL,
                         `status` smallint(6) NOT NULL DEFAULT '10',
                         `created_at` int(11) NOT NULL,
                         `updated_at` int(11) NOT NULL,
                         PRIMARY KEY (`id`),
                         UNIQUE KEY `username` (`username`),
                         UNIQUE KEY `email` (`email`),
                         UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users_appointments_calendarobjects`
--

DROP TABLE IF EXISTS `users_appointments_calendarobjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_appointments_calendarobjects` (
                                                      `users_id` int(11) NOT NULL,
                                                      `appointments_id` int(11) NOT NULL,
                                                      `calendarobjects_id` int(11) unsigned NOT NULL,
                                                      PRIMARY KEY (`users_id`,`appointments_id`,`calendarobjects_id`),
                                                      KEY `idx-users_appointments_calendarobjects-users_id` (`users_id`),
                                                      KEY `idx-users_appointments_calendarobjects-appointments_id` (`appointments_id`),
                                                      KEY `idx-users_appointments_calendarobjects-calendarobjects_id` (`calendarobjects_id`),
                                                      CONSTRAINT `fk-users_appointments_calendarobjects-appointments_id` FOREIGN KEY (`appointments_id`) REFERENCES `appointments` (`id`) ON DELETE CASCADE,
                                                      CONSTRAINT `fk-users_appointments_calendarobjects-calendarobjects_id` FOREIGN KEY (`calendarobjects_id`) REFERENCES `calendarobjects` (`id`) ON DELETE CASCADE,
                                                      CONSTRAINT `fk-users_appointments_calendarobjects-users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users_calendars`
--

DROP TABLE IF EXISTS `users_calendars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_calendars` (
                                   `users_id` int(11) NOT NULL,
                                   `calendars_id` int(11) unsigned NOT NULL,
                                   PRIMARY KEY (`users_id`,`calendars_id`),
                                   KEY `idx-users_calendars-users_id` (`users_id`),
                                   KEY `idx-users_calendars-calendars_id` (`calendars_id`),
                                   CONSTRAINT `fk-users_calendars-calendars_id` FOREIGN KEY (`calendars_id`) REFERENCES `calendars` (`id`) ON DELETE CASCADE,
                                   CONSTRAINT `fk-users_calendars-users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
