/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Dumping data for table `appointments`
--

LOCK TABLES `appointments` WRITE;
/*!40000 ALTER TABLE `appointments`
    DISABLE KEYS */;
INSERT INTO `appointments`
VALUES (4, 'geschlossen, Osterferien', 'Der Kindergarten ist vom 15.04.2019 bis zum 18.04.2019 geschlossen.',
        '2019-04-15', '2019-04-18', NULL, NULL, '', 6),
       (5, '\"Sing mit ...\"',
        '\"Sing mit...\" mit Pfarrer Bäumer. Alle Kinder, Eltern, Großeltern und Interessierte,  die Spaß am Singen haben, sind herzlich ab 14:30 Uhr eingeladen.',
        '2019-04-30', '2019-04-30', '14:30:00', NULL, '', 3),
       (6, 'Polizei im Kindergarten', 'Besuch der Polizei im Kindergarten (Schulanfänger)', '2019-05-06', '2019-05-06',
        '09:00:00', NULL, '', 1),
       (7, 'Selbstverteidigungskurs für Frauen',
        'Selbstverteidigungskurs für Frauen;\r\nweitere Termine:\r\n14.05.2019 20:00\r\n21.05.2019 20:00\r\n25.05.2019 09:30-14:00',
        '2019-05-07', '2019-05-07', '20:00:00', NULL, '', 5),
       (8, 'Selbstverteidigungskurs für Frauen',
        'Selbstverteidigungskurs für Frauen;\r\nweitere Termine:\r\n07.05.2019 20:00\r\n21.05.2019 20:00\r\n25.05.2019 09:30-14:00',
        '2019-05-14', '2019-05-14', '20:00:00', NULL, '', 5),
       (9, 'Selbstverteidigungskurs für Frauen',
        'Selbstverteidigungskurs für Frauen;\r\nweitere Termine:\r\n07.05.2019 20:00\r\n14.05.2019 20:00\r\n25.05.2019 09:30-14:00',
        '2019-05-21', '2019-05-21', '20:00:00', NULL, '', 5),
       (10, 'Selbstverteidigungskurs für Frauen',
        'Selbstverteidigungskurs für Frauen;\r\nweitere Termine:\r\n07.05.2019 20:00\r\n14.05.2019 20:00\r\n21.05.2019 20:00',
        '2019-05-25', '2019-05-25', '09:30:00', '14:00:00', '', 5),
       (11, 'Polizei im Kindergarten \"Wir üben unseren Schulweg\"',
        'Besuch der Polizei im Kindergarten \"Wir üben unseren Schulweg\" (Schulanfänger)', '2019-05-13', '2019-05-13',
        NULL, NULL, '', 1),
       (12, 'geschlossen',
        'Der Kindergarten ist am 17.05.2019 geschlossen. Es findet das Klausurwochenende der Erzieher/Inne statt.',
        '2019-05-17', '2019-05-17', NULL, NULL, '', 6),
       (13, 'Besuch der Polizeistation', 'Besuch der Polizeistation (Schulanfänger)', '2019-05-20', '2019-05-20',
        '14:00:00', NULL, '', 1),
       (14, 'Elterntreff', 'Thema wird bekanntgegeben', '2019-05-23', '2019-05-23', '14:00:00', '14:00:00', '', 5);
/*!40000 ALTER TABLE `appointments`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `appointments_groups`
--

LOCK TABLES `appointments_groups` WRITE;
/*!40000 ALTER TABLE `appointments_groups`
    DISABLE KEYS */;
INSERT INTO `appointments_groups`
VALUES (4, 1),
       (4, 2),
       (4, 3),
       (4, 4),
       (5, 1),
       (5, 2),
       (5, 3),
       (5, 4),
       (6, 2),
       (6, 4),
       (7, 1),
       (7, 2),
       (7, 3),
       (7, 4),
       (8, 1),
       (8, 2),
       (8, 3),
       (8, 4),
       (9, 1),
       (9, 2),
       (9, 3),
       (9, 4),
       (10, 1),
       (10, 2),
       (10, 3),
       (10, 4),
       (11, 2),
       (11, 4),
       (12, 1),
       (12, 2),
       (12, 3),
       (12, 4),
       (13, 2),
       (13, 4),
       (14, 1),
       (14, 2),
       (14, 3),
       (14, 4);
/*!40000 ALTER TABLE `appointments_groups`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `calendarchanges`
--

LOCK TABLES `calendarchanges` WRITE;
/*!40000 ALTER TABLE `calendarchanges`
    DISABLE KEYS */;
INSERT INTO `calendarchanges`
VALUES (1, _binary '07c06ab9-1c6a-f391-2ec4-025fa94d470b.1555251406.0731.ics', 1, 1, 1),
       (2, _binary '07c06ab9-1c6a-f391-2ec4-025fa94d470b.1555251406.0731.ics', 2, 1, 2),
       (3, _binary 'b1df8a12-dfe9-d3c9-b7f3-fcc7ad090a46.1555251410.3264.ics', 3, 1, 1),
       (4, _binary 'b1df8a12-dfe9-d3c9-b7f3-fcc7ad090a46.1555251410.3264.ics', 4, 1, 2),
       (5, _binary 'b1df8a12-dfe9-d3c9-b7f3-fcc7ad090a46.1555251410.3264.ics', 5, 1, 2),
       (6, _binary 'b1df8a12-dfe9-d3c9-b7f3-fcc7ad090a46.1555251410.3264.ics', 6, 1, 2),
       (7, _binary 'b1df8a12-dfe9-d3c9-b7f3-fcc7ad090a46.1555251410.3264.ics', 7, 1, 2),
       (8, _binary 'b1df8a12-dfe9-d3c9-b7f3-fcc7ad090a46.1555251410.3264.ics', 8, 1, 2),
       (9, _binary '6d0a7da5-3070-339d-ced7-bd09a25ec259.1555254612.5813.ics', 9, 1, 1),
       (10, _binary '6d0a7da5-3070-339d-ced7-bd09a25ec259.1555254612.5813.ics', 10, 1, 2),
       (11, _binary '6d0a7da5-3070-339d-ced7-bd09a25ec259.1555254612.5813.ics', 11, 1, 2),
       (12, _binary '4e1be31e-75f0-6059-a61d-118a4a375d2b.1555255125.8832.ics', 12, 1, 1),
       (13, _binary 'af69dacd-6355-1bd6-6620-5dc1b6f9b2ce.1555255163.324.ics', 13, 1, 1),
       (14, _binary 'd8f54528-fe47-2057-625d-867620f16586.1555255198.7547.ics', 14, 1, 1),
       (15, _binary 'dbbb12d5-c8be-fe3e-0388-cf5fad77a78a.1555255287.4308.ics', 15, 1, 1),
       (16, _binary '65eba93d-0fd7-92de-450a-c8165ebe2a42.1555255543.4684.ics', 16, 1, 1),
       (17, _binary '07c06ab9-1c6a-f391-2ec4-025fa94d470b.1555251406.0731.ics', 17, 1, 2),
       (18, _binary '07c06ab9-1c6a-f391-2ec4-025fa94d470b.1555251406.0731.ics', 18, 1, 2),
       (19, _binary '66df7cff-6380-493b-b907-7432989c8d52.1555255707.2146.ics', 19, 1, 1),
       (20, _binary '8852db4d-1d12-d1e0-e78b-d563f6c026f1.1555255782.524.ics', 20, 1, 1),
       (21, _binary 'f71edf29-5795-28c5-e3c2-8222ae1827a9.1555258198.5397.ics', 21, 1, 1),
       (22, '', 1, 2, 2);
/*!40000 ALTER TABLE `calendarchanges`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `calendarinstances`
--

LOCK TABLES `calendarinstances` WRITE;
/*!40000 ALTER TABLE `calendarinstances`
    DISABLE KEYS */;
INSERT INTO `calendarinstances`
VALUES (1, 1, _binary 'principals/muvshot', 1, 'Johannes Kindergarten',
        _binary 'B5885FE3-0E50-9E87-6F05-8023897A7F72.ics', 'Public Calendar with main Events.', 0, _binary '#FF2968',
        'BEGIN:VCALENDAR\nVERSION:2.0\nCALSCALE:GREGORIAN\nBEGIN:VTIMEZONE\nTZID:Europe/Berlin\nBEGIN:DAYLIGHT\nTZOFFSETFROM:+0100\nRRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU\nDTSTART:19810329T020000\nTZNAME:MESZ\nTZOFFSETTO:+0200\nEND:DAYLIGHT\nBEGIN:STANDARD\nTZOFFSETFROM:+0200\nRRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU\nDTSTART:19961027T030000\nTZNAME:MEZ\nTZOFFSETTO:+0100\nEND:STANDARD\nEND:VTIMEZONE\nEND:VCALENDAR',
        0, NULL, NULL, 2);
/*!40000 ALTER TABLE `calendarinstances`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `calendarobjects`
--

LOCK TABLES `calendarobjects` WRITE;
/*!40000 ALTER TABLE `calendarobjects`
    DISABLE KEYS */;
INSERT INTO `calendarobjects`
VALUES (1,
        _binary 'BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//Sabre//Sabre VObject 4.2.0//EN\r\nCALSCALE:GREGORIAN\r\nBEGIN:VEVENT\r\nUID:sabre-vobject-bb288b58-6ef8-4cc8-94a7-10180880c927\r\nDTSTAMP:20190414T152647Z\r\nSUMMARY:geschlossen\\, Osterferien\r\nDESCRIPTION:Der Kindergarten ist vom 15.04.2019 bis zum 18.04.2019 geschlos\r\n sen.\r\nDTSTART:20190415T000000Z\r\nDTEND:20190418T000000Z\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n',
        _binary '07c06ab9-1c6a-f391-2ec4-025fa94d470b.1555251406.0731.ics', 1, 1555255607,
        _binary '0b98010242e9f362f5a1b51afa10603b', 384, _binary 'VEVENT', 1555286400, 1555545600,
        _binary 'sabre-vobject-bb288b58-6ef8-4cc8-94a7-10180880c927'),
       (2,
        _binary 'BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//Sabre//Sabre VObject 4.2.0//EN\r\nCALSCALE:GREGORIAN\r\nBEGIN:VEVENT\r\nUID:sabre-vobject-b15a16e0-bff1-49d8-b3f8-1583e657b285\r\nDTSTAMP:20190414T141703Z\r\nSUMMARY:\"Sing mit ...\"\r\nDESCRIPTION:\"Sing mit...\" mit Pfarrer Bäumer. Alle Kinder\\, Eltern\\, Groß\r\n eltern und Interessierte\\,  die Spaß am Singen haben\\, sind herzlich ab 1\r\n 4:30 Uhr eingeladen.\r\nDTSTART:20190430T143000Z\r\nDTEND:20190430T000000Z\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n',
        _binary 'b1df8a12-dfe9-d3c9-b7f3-fcc7ad090a46.1555251410.3264.ics', 1, 1555251423,
        _binary '2bfb92dbeb55b21753857782468905f4', 466, _binary 'VEVENT', 1556634600, 1556582400,
        _binary 'sabre-vobject-b15a16e0-bff1-49d8-b3f8-1583e657b285'),
       (3,
        _binary 'BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//Sabre//Sabre VObject 4.2.0//EN\r\nCALSCALE:GREGORIAN\r\nBEGIN:VEVENT\r\nUID:sabre-vobject-9b0df0ee-6391-44cd-bca5-5e0b281c4fcc\r\nDTSTAMP:20190414T151059Z\r\nSUMMARY:Polizei im Kindergarten\r\nDESCRIPTION:Besuch der Polizei im Kindergarten (Schulanfänger)\r\nDTSTART:20190506T090000Z\r\nDTEND:20190506T000000Z\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n',
        _binary '6d0a7da5-3070-339d-ced7-bd09a25ec259.1555254612.5813.ics', 1, 1555254659,
        _binary 'e2bcb3736fc3f514bf5b88dc04f40e25', 363, _binary 'VEVENT', 1557133200, 1557100800,
        _binary 'sabre-vobject-9b0df0ee-6391-44cd-bca5-5e0b281c4fcc'),
       (4,
        _binary 'BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//Sabre//Sabre VObject 4.2.0//EN\r\nCALSCALE:GREGORIAN\r\nBEGIN:VEVENT\r\nUID:sabre-vobject-21b65347-4634-46ab-951c-d0d755f69102\r\nDTSTAMP:20190414T151845Z\r\nSUMMARY:Selbstverteidigungskurs für Frauen\r\nDESCRIPTION:Selbstverteidigungskurs für Frauen\\;\\nweitere Termine:\\n14.05.\r\n 2019 20:00\\n21.05.2019 20:00\\n25.05.2019 09:30-14:00\r\nDTSTART:20190507T200000Z\r\nDTEND:20190507T000000Z\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n',
        _binary '4e1be31e-75f0-6059-a61d-118a4a375d2b.1555255125.8832.ics', 1, 1555255125,
        _binary 'ba70b3b478090b9a54f6174db23d06c7', 442, _binary 'VEVENT', 1557259200, 1557187200,
        _binary 'sabre-vobject-21b65347-4634-46ab-951c-d0d755f69102'),
       (5,
        _binary 'BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//Sabre//Sabre VObject 4.2.0//EN\r\nCALSCALE:GREGORIAN\r\nBEGIN:VEVENT\r\nUID:sabre-vobject-c7b2e681-52ad-4d16-9231-f64ed29c326b\r\nDTSTAMP:20190414T151923Z\r\nSUMMARY:Selbstverteidigungskurs für Frauen\r\nDESCRIPTION:Selbstverteidigungskurs für Frauen\\;\\nweitere Termine:\\n07.05.\r\n 2019 20:00\\n21.05.2019 20:00\\n25.05.2019 09:30-14:00\r\nDTSTART:20190514T200000Z\r\nDTEND:20190514T000000Z\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n',
        _binary 'af69dacd-6355-1bd6-6620-5dc1b6f9b2ce.1555255163.324.ics', 1, 1555255163,
        _binary '04adf93e0c5d172d4247d675f7dce560', 442, _binary 'VEVENT', 1557864000, 1557792000,
        _binary 'sabre-vobject-c7b2e681-52ad-4d16-9231-f64ed29c326b'),
       (6,
        _binary 'BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//Sabre//Sabre VObject 4.2.0//EN\r\nCALSCALE:GREGORIAN\r\nBEGIN:VEVENT\r\nUID:sabre-vobject-a6c59769-0a81-43cf-ab76-3ee38443cfd1\r\nDTSTAMP:20190414T151958Z\r\nSUMMARY:Selbstverteidigungskurs für Frauen\r\nDESCRIPTION:Selbstverteidigungskurs für Frauen\\;\\nweitere Termine:\\n07.05.\r\n 2019 20:00\\n14.05.2019 20:00\\n25.05.2019 09:30-14:00\r\nDTSTART:20190521T200000Z\r\nDTEND:20190521T000000Z\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n',
        _binary 'd8f54528-fe47-2057-625d-867620f16586.1555255198.7547.ics', 1, 1555255198,
        _binary '92820cc99a6cab0f9fcc4d10f8a39132', 442, _binary 'VEVENT', 1558468800, 1558396800,
        _binary 'sabre-vobject-a6c59769-0a81-43cf-ab76-3ee38443cfd1'),
       (7,
        _binary 'BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//Sabre//Sabre VObject 4.2.0//EN\r\nCALSCALE:GREGORIAN\r\nBEGIN:VEVENT\r\nUID:sabre-vobject-7f93a31c-ce8b-41bd-b3ad-0feda3884543\r\nDTSTAMP:20190414T152127Z\r\nSUMMARY:Selbstverteidigungskurs für Frauen\r\nDESCRIPTION:Selbstverteidigungskurs für Frauen\\;\\nweitere Termine:\\n07.05.\r\n 2019 20:00\\n14.05.2019 20:00\\n21.05.2019 20:00\r\nDTSTART:20190525T093000Z\r\nDTEND:20190525T140000Z\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n',
        _binary 'dbbb12d5-c8be-fe3e-0388-cf5fad77a78a.1555255287.4308.ics', 1, 1555255287,
        _binary '3f48996017fa55b57f8027f4808c1ad6', 436, _binary 'VEVENT', 1558776600, 1558792800,
        _binary 'sabre-vobject-7f93a31c-ce8b-41bd-b3ad-0feda3884543'),
       (8,
        _binary 'BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//Sabre//Sabre VObject 4.2.0//EN\r\nCALSCALE:GREGORIAN\r\nBEGIN:VEVENT\r\nUID:sabre-vobject-a4ffa0c4-a11d-41e6-99f7-8942d465d21a\r\nDTSTAMP:20190414T152543Z\r\nSUMMARY:Polizei im Kindergarten \"Wir üben unseren Schulweg\"\r\nDESCRIPTION:Besuch der Polizei im Kindergarten \"Wir üben unseren Schulweg\"\r\n  (Schulanfänger)\r\nDTSTART:20190513T000000Z\r\nDTEND:20190513T000000Z\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n',
        _binary '65eba93d-0fd7-92de-450a-c8165ebe2a42.1555255543.4684.ics', 1, 1555255543,
        _binary 'ad25fc43ad2551ae579e888d0dc18e8e', 424, _binary 'VEVENT', 1557705600, 1557705600,
        _binary 'sabre-vobject-a4ffa0c4-a11d-41e6-99f7-8942d465d21a'),
       (9,
        _binary 'BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//Sabre//Sabre VObject 4.2.0//EN\r\nCALSCALE:GREGORIAN\r\nBEGIN:VEVENT\r\nUID:sabre-vobject-e216b598-33c2-4305-8547-9aef4481c4e6\r\nDTSTAMP:20190414T152827Z\r\nSUMMARY:geschlossen\r\nDESCRIPTION:Der Kindergarten ist am 17.05.2019 geschlossen. Es findet das K\r\n lausurwochenende der Erzieher/Inne statt.\r\nDTSTART:20190517T000000Z\r\nDTEND:20190517T000000Z\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n',
        _binary '66df7cff-6380-493b-b907-7432989c8d52.1555255707.2146.ics', 1, 1555255707,
        _binary 'bef52c397f10b0c213818ed487106788', 407, _binary 'VEVENT', 1558051200, 1558051200,
        _binary 'sabre-vobject-e216b598-33c2-4305-8547-9aef4481c4e6'),
       (10,
        _binary 'BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//Sabre//Sabre VObject 4.2.0//EN\r\nCALSCALE:GREGORIAN\r\nBEGIN:VEVENT\r\nUID:sabre-vobject-c8d7fc2d-6949-46f5-a5f6-0a3c79630132\r\nDTSTAMP:20190414T152942Z\r\nSUMMARY:Besuch der Polizeistation\r\nDESCRIPTION:Besuch der Polizeistation (Schulanfänger)\r\nDTSTART:20190520T140000Z\r\nDTEND:20190520T000000Z\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n',
        _binary '8852db4d-1d12-d1e0-e78b-d563f6c026f1.1555255782.524.ics', 1, 1555255782,
        _binary '94b670f90d490ada6970fe8c6800cc54', 356, _binary 'VEVENT', 1558360800, 1558310400,
        _binary 'sabre-vobject-c8d7fc2d-6949-46f5-a5f6-0a3c79630132'),
       (11,
        _binary 'BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:-//Sabre//Sabre VObject 4.2.0//EN\r\nCALSCALE:GREGORIAN\r\nBEGIN:VEVENT\r\nUID:sabre-vobject-b3012e1b-bf87-458c-a722-46078e1d49eb\r\nDTSTAMP:20190414T160958Z\r\nSUMMARY:Elterntreff\r\nDESCRIPTION:Thema wird bekanntgegeben\r\nDTSTART:20190523T140000Z\r\nDTEND:20190523T140000Z\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n',
        _binary 'f71edf29-5795-28c5-e3c2-8222ae1827a9.1555258198.5397.ics', 1, 1555258198,
        _binary '3f0e17a657109dd4e4da6c50aceb40fa', 325, _binary 'VEVENT', 1558620000, 1558620000,
        _binary 'sabre-vobject-b3012e1b-bf87-458c-a722-46078e1d49eb');
/*!40000 ALTER TABLE `calendarobjects`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `calendars`
--

LOCK TABLES `calendars` WRITE;
/*!40000 ALTER TABLE `calendars`
    DISABLE KEYS */;
INSERT INTO `calendars`
VALUES (1, 22, _binary 'VEVENT');
/*!40000 ALTER TABLE `calendars`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories`
    DISABLE KEYS */;
INSERT INTO `categories`
VALUES (1, 'intern', 'nur für die Kinder', '#5dbcfc'),
       (2, 'intern mit Elternunterstützung', 'nur für die Kinder mit Hilfestellung durch die Eltern', '#5dfc87'),
       (3, 'halb öffentlich', 'mit den Eltern der Kinder', '#e3fc76'),
       (4, 'öffentlich', 'Es sind alle Interessierten Personen eingeladen', '#fc7f76'),
       (5, 'Eltern', 'nur für Eltern', '#fccb76'),
       (6, 'geschlossen', 'In diesem Zeit ist der Kindergarten geschlossen', '#ff6347');
/*!40000 ALTER TABLE `categories`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `categories_users`
--

LOCK TABLES `categories_users` WRITE;
/*!40000 ALTER TABLE `categories_users`
    DISABLE KEYS */;
INSERT INTO `categories_users`
VALUES (1, 1),
       (2, 1),
       (3, 1),
       (4, 1),
       (5, 1),
       (6, 1);
/*!40000 ALTER TABLE `categories_users`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups`
    DISABLE KEYS */;
INSERT INTO `groups`
VALUES (1, 'rot', 'rote Gruppe', '#ff0000'),
       (2, 'gelb', 'gelbe Gruppe', '#ffff00'),
       (3, 'grün', 'grüne Gruppe', '#00ff00'),
       (4, 'blau', 'blaue Gruppe', '#0000ff');
/*!40000 ALTER TABLE `groups`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `groups_users`
--

LOCK TABLES `groups_users` WRITE;
/*!40000 ALTER TABLE `groups_users`
    DISABLE KEYS */;
INSERT INTO `groups_users`
VALUES (1, 1),
       (2, 1),
       (3, 1),
       (4, 1);
/*!40000 ALTER TABLE `groups_users`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration`
    DISABLE KEYS */;
INSERT INTO `migration`
VALUES ('m000000_000000_base', 1551021881),
       ('m130524_201442_init', 1551021883),
       ('m180927_144305_migrate_sabre_dav', 1551021886),
       ('m180929_084406_create_categories_table', 1551021886),
       ('m180929_104836_create_groups_table', 1551021886),
       ('m180929_104900_create_appointments_table', 1551021887),
       ('m180929_104932_create_junction_table_for_appointments_and_groups_tables', 1551021887),
       ('m180930_141658_create_junction_table_for_categories_and_users_tables', 1551021888),
       ('m180930_141708_create_junction_table_for_groups_and_users_tables', 1551021889),
       ('m180930_141801_create_junction_table_for_users_and_appointments_and_calendarobjects_tables', 1551021891),
       ('m180930_161830_add_1_column_to_categories_table', 1551021891),
       ('m181005_090921_create_junction_table_for_users_and_calendars_tables', 1551021892);
/*!40000 ALTER TABLE `migration`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `principals`
--

LOCK TABLES `principals` WRITE;
/*!40000 ALTER TABLE `principals`
    DISABLE KEYS */;
INSERT INTO `principals`
VALUES (1, _binary 'principals/muvshot', _binary 'muvshot@example.de', 'muvshot'),
       (2, _binary 'principals/muvshot/calendar-proxy-read', NULL, NULL),
       (3, _binary 'principals/muvshot/calendar-proxy-write', NULL, NULL);
/*!40000 ALTER TABLE `principals`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users`
    DISABLE KEYS */;
INSERT INTO `users`
VALUES (1, 'muvshot', NULL, '$2y$13$skvPL4H6iP0MvcrfKGUwT.7Yg6FqQS4LhuJADrrQABcJGUNpaT1KO', NULL,
        'muvshot@example.de', 10, 1552234097, 1555251243);
/*!40000 ALTER TABLE `users`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users_appointments_calendarobjects`
--

LOCK TABLES `users_appointments_calendarobjects` WRITE;
/*!40000 ALTER TABLE `users_appointments_calendarobjects`
    DISABLE KEYS */;
INSERT INTO `users_appointments_calendarobjects`
VALUES (1, 4, 1),
       (1, 5, 2),
       (1, 6, 3),
       (1, 7, 4),
       (1, 8, 5),
       (1, 9, 6),
       (1, 10, 7),
       (1, 11, 8),
       (1, 12, 9),
       (1, 13, 10),
       (1, 14, 11);
/*!40000 ALTER TABLE `users_appointments_calendarobjects`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users_calendars`
--

LOCK TABLES `users_calendars` WRITE;
/*!40000 ALTER TABLE `users_calendars`
    DISABLE KEYS */;
INSERT INTO `users_calendars`
VALUES (1, 1);
/*!40000 ALTER TABLE `users_calendars`
    ENABLE KEYS */;
UNLOCK TABLES;


/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2019-05-01 21:08:18
