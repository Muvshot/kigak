<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'Appointment management Kindergarten' => '',
    'Appointments' => '',
    'Don\'t rely on this information, the information can be outdated.' => '',
    'Hello dear Visitor.' => '',
    'I will discuss this topic with the parent association, if i\'m able to add value the community.' => '',
    'It is run by me, a parent, on my own authority, without the management of the kindergarten.' => '',
    'It\'s for my personal use and still in development.' => '',
    'Reset' => 'Reset',
    'Search' => 'Suche',
    'This is a private site for managing the appointments in the Johannes-Kindergarten in Meschede, Germany.' => '',
    'upcoming Appointments' => 'bevorstehende Termine',
];
