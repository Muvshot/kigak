<?php
namespace frontend\models;

use common\helper\CalendarHelper;
use common\models\Category;
use common\models\Group;
use common\models\User as CommonUser;
use common\models\UsersAppointmentsCalendarobjects;
use Throwable;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * 
 * @property Category[] $categories
 * @property Group[] $groups
 * @property UsersAppointmentsCalendarobjects[] $usersAppointmentsCalendarobjects
 */
class User extends CommonUser
{

    /**
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function updateCalendarAbos()
    {
        
        $newAppointments = Appointment::find()->byCategory(array_keys($this->categories))->byGroup(array_keys($this->groups))->indexBy('id')->all();
        /** @var UsersAppointmentsCalendarobjects[] $oldAppointmentsCalendarobjects */
        $oldAppointmentsCalendarobjects = ArrayHelper::index($this->usersAppointmentsCalendarobjects, 'appointments_id');
        $newAppointmentKeys = array_keys($newAppointments);
        $oldAppointmentKeys = array_keys($oldAppointmentsCalendarobjects);
        $toCreateIds = array_diff($newAppointmentKeys, $oldAppointmentKeys);
        $toDeleteIds = array_diff($oldAppointmentKeys, $newAppointmentKeys);

        foreach($toCreateIds as $toCreateId) {
            $calendarobjectId = CalendarHelper::createCalendarObject($this->calendarId, $newAppointments[$toCreateId]);
            $this->addUsersAppointmentsCalendarobject($toCreateId, $calendarobjectId);
        }
        foreach($toDeleteIds as $toDeleteId) {
            CalendarHelper::deleteCalendarObject($this->calendarId, $oldAppointmentsCalendarobjects[$toDeleteId]);
            $oldAppointmentsCalendarobjects[$toDeleteId]->delete();
        }
    }
}