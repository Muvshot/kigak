<?php
namespace frontend\models;



use Yii;

/**
 *
 * @property-read $dateInfo string
 *
 */
class Appointment extends \common\models\Appointment
{
    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getDateInfo()
    {
        $dateInfo = Yii::$app->formatter->asDate($this->beginDate);
        if($this->beginDate<$this->endDate){
            $dateInfo .= ' - '.Yii::$app->formatter->asDate($this->endDate);
        }
        if(isset($this->beginTime) && isset($this->endTime))
        {
            $dateInfo .= ', '.$this->beginTime.' - '.$this->endTime;
        }
        
        return $dateInfo;
    }
}