<?php

namespace frontend\models\search;

use frontend\models\Appointment;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AppointmentSearch represents the model behind the search form of `common\models\Appointment`.
 */
class AppointmentSearch extends Appointment
{
    public $searchDate;

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \Exception
     */
    public function init()
    {
        parent::init();
        $this->searchDate = Yii::$app->formatter->asDate(new \DateTime(),'Y-M-d');
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'searchDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Appointment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andWhere(['>=', 'endDate', $this->searchDate]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description]);
        
        $query->orderBy('beginDate');

        return $dataProvider;
    }
}
