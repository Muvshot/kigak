<?php
/**
 * Created by PhpStorm.
 * User: php-dev
 * Date: 17.08.18
 * Time: 03:11
 */


/* @var $model backend\models\Appointment */
/* @var $key mixed */
/* @var $index integer */
/* @var $widget \yii\widgets\ListView */

?>


<div class="panel panel-default">
    <div class="panel-heading" style="background-color: <?= $model->category->html_color_code ?>">
        <h3 class="panel-title">
            <?= $model->title ?>
        </h3>
    </div>
    <div class="panel-body">
        <h4><?= $model->dateInfo; ?></h4>
        <p><?= $model->description ?></p>
    </div>
    <div class="panel-footer"><?= implode(' ', $model->groups)?></div>
</div>