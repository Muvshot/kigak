<?php

/* @var $this yii\web\View */

$this->title = Yii::t('frontend','Appointment management Kindergarten');
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= Yii::t('frontend', 'Hello dear Visitor.') ?></h1>

        <p class="lead">
            <?= Yii::t('frontend', 'This is a private site for managing the appointments in the Johannes-Kindergarten in Meschede, Germany.') ?>
            <?= Yii::t('frontend', 'It is run by me, a parent, on my own authority, without the management of the kindergarten.') ?>
            <?= Yii::t('frontend', 'It\'s for my personal use and still in development.') ?>
            <?= Yii::t('frontend', 'Don\'t rely on this information, the information can be outdated.') ?>
            <?= Yii::t('frontend', 'I will discuss this topic with the parent association, if i\'m able to add value the community.') ?>
        </p>

        <p><a class="btn btn-lg btn-success" href="/appointment/"><?= Yii::t('frontend', 'Appointments') ?></a></p>
    </div>
    
</div>
