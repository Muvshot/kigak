<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\User */

use common\models\Category;
use common\models\Group;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = 'Abonoments';
$this->params['breadcrumbs'][] = $this->title;
$possibleCategories = Category::find()->all();
$possibleGroups = Group::find()->all();
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please select the Categories and Groups you want to follow:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-abos']); ?>

                <?= $form->field($model, 'categories')->widget(Select2::class, [
                    'data' => ArrayHelper::map($possibleCategories, 'id', 'category'),
                    'options' => [
                        'placeholder' => Yii::t('frontend', 'Please select one or more Categories ...'),
                        'multiple' => true,
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>

                <?= $form->field($model, 'groups')->widget(Select2::class, [
                    'data' => ArrayHelper::map($possibleGroups, 'id', 'group'),
                    'options' => [
                        'placeholder' => Yii::t('frontend', 'Please select one or more Groups ...'),
                        'multiple' => true,
                        'options' => ArrayHelper::map($possibleGroups, 'id', 'htmlOptions'),
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'submit-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
