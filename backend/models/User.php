<?php
namespace backend\models;

use common\models\Calendar;
use common\models\Calendarinstance;
use common\models\Principal;
use common\models\User as CommonUser;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\Exception;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * 
 * @property /common/models/Category[] $categories
 * @property /common/models/Group[] $groups
 * @property /common/models/UsersAppointmentsCalendarobjects[] $usersAppointmentsCalendarobjects
 */
class User extends CommonUser
{
    /**
     * @return bool
     */
    public function activate()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try{
            $this->status = self::STATUS_ACTIVE;
            $this->save();
            
            $this->createPrincipals();
            $this->createCalendar();
            $transaction->commit();
            return true;
        } catch(Exception $e) {
            Yii::error($e->getMessage(), __METHOD__);
            $transaction->rollback();
            return false;
        }
    }
    
    protected function createPrincipals()
    {
        $principal = new Principal([
            'uri' => $this->getRootPrincipalPath(),
            'email' => $this->email,
            'displayname' => $this->username,
        ]);
        $principal->save();
        $principal = new Principal([
            'uri' => $this->getRootPrincipalPath().'/calendar-proxy-read',
        ]);
        $principal->save();
        $principal = new Principal([
            'uri' => $this->getRootPrincipalPath().'/calendar-proxy-write',
        ]);
        $principal->save();
    }
    
    protected function createCalendar()
    {
        $calendar = new Calendar(['components' => 'VEVENT']);
        $calendar->save();
        $calendar->refresh();
        
        $this->link('calendars', $calendar);
        
        $calendarinstance = new Calendarinstance([
            'calendarid' => $calendar->id,
            'principaluri' => $this->getRootPrincipalPath(),
            'displayname' => Calendarinstance::MAIN_DISPLAY_NAME,
            'description' => 'Public Calendar with main Events.',
            'uri' => Calendarinstance::createUid().'.ics',
        ]);
        $calendarinstance->save();
    }

    /**
     * @return bool
     * @throws InvalidConfigException
     */
    public function getHasCalendars()
    {
        return $this->getCalendars()->count()>0;
    }
    
    /**
     * {@inheritdoc}
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, ['>=', 'status', self::STATUS_SYSADMIN]]);
    }
}