<?php

namespace backend\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[User]].
 *
 * @see User
 */
class UserQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return User[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return User|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param string $username
     * @return UserQuery
     */
    public function byUsername(string $username)
    {
        return $this->andWhere(['[[username]]' => $username]);
    }

    /**
     * @param int $status
     * @return UserQuery
     */
    public function minStatus(int $status)
    {
        return $this->andWhere(['>=', '[[status]]', $status]);
    }
}
