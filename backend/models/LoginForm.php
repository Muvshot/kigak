<?php
namespace backend\models;

use common\models\LoginForm as CommonLoginForm;

/**
 * Login form
 */
class LoginForm extends CommonLoginForm
{
    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::find()->byUsername($this->username)->minStatus(USER::STATUS_SYSADMIN)->one();
        }

        return $this->_user;
    }
}
