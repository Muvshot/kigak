<?php

namespace backend\models\search;

use backend\models\Appointment;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AppointmentSearch represents the model behind the search form of `backend\models\Appointment`.
 */
class AppointmentSearch extends Appointment
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'description', 'beginDate', 'endDate', 'beginTime', 'endTime', 'repeatInterval'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Appointment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'beginDate' => $this->beginDate,
            'endDate' => $this->endDate,
            'beginTime' => $this->beginTime,
            'endTime' => $this->endTime,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'repeatInterval', $this->repeatInterval]);

        return $dataProvider;
    }
}
