<?php
namespace backend\models;

use common\helper\CalendarHelper;
use common\models\User;
use common\models\UsersAppointmentsCalendarobjects;
use Yii;
use yii\helpers\ArrayHelper;

/**
 *
 * @property-read $dateInfo string
 *
 */
class Appointment extends \common\models\Appointment
{
    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function getDateInfo()
    {
        $dateInfo = Yii::$app->formatter->asDate($this->beginDate);
        if($this->beginDate<$this->endDate){
            $dateInfo .= ' - '.Yii::$app->formatter->asDate($this->endDate);
        }
        if(isset($this->beginTime) && isset($this->endTime))
        {
            $dateInfo .= ', '.$this->beginTime.' - '.$this->endTime;
        }
        
        return $dateInfo;
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::class, ['appointments_id' => 'id']);
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function updateAbos()
    {
        $newUsers = User::find()->byCategory($this->categories_id)->byGroups(array_keys($this->groups))->indexBy('id')->all();
        /** @var UsersAppointmentsCalendarobjects[] $existingUsers */
        $existingUsers = ArrayHelper::index($this->usersAppointmentsCalendarobjects, 'users_id');
        
        $newUsersKeys = array_keys($newUsers);
        $existingUsersKeys = array_keys($existingUsers);
        $toDeleteIds = array_diff($existingUsersKeys, $newUsersKeys);
        
        foreach ($newUsers as $newUser)
        {
            if(($userAppointmentCalendarObject=$newUser->getAppointmentCalendarObject($this->id))!==null) {
                //needs update?
                if($userAppointmentCalendarObject->calendarobject->etag!=CalendarHelper::createEtag($this->davCalendardata->serialize())) {
                    CalendarHelper::updateCalendarObject($newUser->calendarId, $userAppointmentCalendarObject);
                }
            } else {
                $calendarobjectId = CalendarHelper::createCalendarObject($newUser->calendarId, $this);
                $newUser->addUsersAppointmentsCalendarobject($this->id, $calendarobjectId);
            }
        }
        
        foreach($toDeleteIds as $toDeleteId) {
            CalendarHelper::deleteCalendarObject($existingUsers[$toDeleteId]->user->calendarId, $existingUsers[$toDeleteId]);
            $existingUsers[$toDeleteId]->delete();
        }
        
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function deleteAbos()
    {
        /** @var UsersAppointmentsCalendarobjects[] $existingUsers */
        $existingUsers = ArrayHelper::index($this->usersAppointmentsCalendarobjects, 'users_id');
        foreach ($existingUsers as $existingUser)
        {
            CalendarHelper::deleteCalendarObject($existingUser->user->calendarId, $existingUser);
            $existingUser->delete();
        }

    }
}

