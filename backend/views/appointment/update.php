<?php


/* @var $this yii\web\View */
/* @var $model backend\models\Appointment */

$this->title = Yii::t('backend', 'Update Appointment: {nameAttribute} ', ['nameAttribute' => '' . $model->title ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Appointments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="appointment-update">

    <!-- <h1 class="col-xs-12"><?= ''//Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
