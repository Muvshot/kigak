<?php


use common\models\Category;
use common\models\Group;
use kartik\datecontrol\DateControl;
use kartik\time\TimePicker;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Appointment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appointment-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'title', ['options' => ['class' => 'form-group col-xs-12']])
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description', ['options' => ['class' => 'form-group col-xs-12']])
        ->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'beginDate', ['options' => ['class' => 'form-group col-md-6 col-xs-12']])
        ->widget(DateControl::class, ['type' => DateControl::FORMAT_DATE]); ?>
    
    <?= $form->field($model, 'endDate', ['options' => ['class' => 'form-group col-md-6 col-xs-12']])
        ->widget(DateControl::class, ['type' => DateControl::FORMAT_DATE]); ?>

    <?= $form->field($model, 'beginTime', ['options' => ['class' => 'form-group col-md-6 col-xs-12']])
        ->widget(TimePicker::class, ['pluginOptions' => ['defaultTime' => false, 'showMeridian' => false]]) ?>

    <?= $form->field($model, 'endTime', ['options' => ['class' => 'form-group col-md-6 col-xs-12']])
        ->widget(TimePicker::class, ['pluginOptions' => ['defaultTime' => false, 'showMeridian' => false]]) ?>

    <?= $form->field($model, 'repeatInterval', ['options' => ['class' => 'form-group 6 col-xs-12']])
        ->textInput(['maxlength' => true]) ?>
        
    <?= $form->field($model, 'categories_id', ['options' => ['class' => 'form-group 6 col-xs-12']])
        ->dropDownList(ArrayHelper::map(Category::find()->all(), 'id', 'category')) ?>
        
    <?= $form->field($model, 'groups', ['options' => ['class' => 'form-group 6 col-xs-12']])
        ->widget(Select2::class, [
            'data' => ArrayHelper::map(Group::find()->all(), 'id', 'group'),
            'options' => [
                'placeholder' => Yii::t('backend', 'Please select one or more Groups ...'),
                'multiple' => true,
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>

    <div class="form-group col-xs-12">
        <?= Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
