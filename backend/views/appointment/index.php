<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AppointmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Appointments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appointment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Appointment'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'beginDate:date',
            'endDate:date',
            'title',
            'description:ntext',
            'beginTime:time',
            'endTime:time',
            'repeatInterval',
            [
                'attribute' => 'category',
                'contentOptions' => function ($model, $key, $index, $column) {
                    return ['style' => 'background-color:'.$model->category->html_color_code];
                }
            ],
            [
                'attribute' => 'groups',
                'content' => function ($model, $key, $index, $column) {
                    return implode(' ', $model->groups);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {duplicate} {delete}',
                'contentOptions' => ['class' => 'text-nowrap'],
                'buttons' => [
                    'duplicate' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-duplicate"></span>', Url::to(['create', 'id' => $model->id]), [
                            'title' => Yii::t('yii', 'Duplicate'),
                            'data-pjax' => '0',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
