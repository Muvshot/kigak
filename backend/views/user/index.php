<?php

use backend\models\User;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('backend', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                'username',
                'email:email',
                'status',
                'created_at:datetime',
                'updated_at:datetime',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['class' => 'text-nowrap lead'],
                    'template' => '{activate} {update} {delete}',
                    'buttons' => [
                        'activate' => function (string $url, User $model) {
                            if ($model->status < User::STATUS_ACTIVE) {
                                return Html::a('<span class="glyphicon glyphicon-off text-danger"></span>', $url, ['data-method' => 'post', 'data-pjax' => '0']);
                            } else {
                                return '<span class="glyphicon glyphicon-off text-success"></span>';
                            }
                        },
                    ]
                ],
            ],
        ]);
    } catch (Exception $e) { ?>
        <h2 class="text-danger"><?= Yii::t('backend', 'Can\'t render the GridView-Widget.')?></h2>
        <p><?= $e->getMessage() ?></p>
    <?php } ?>
</div>
