<?php
namespace common\helper;

use common\models\Appointment;
use common\models\Calendarobject;
use common\models\UsersAppointmentsCalendarobjects;
use Sabre\CalDAV\Backend\PDO;

class CalendarHelper
{
    public static function createCalendarObject(int $calendarId, Appointment $appointment)
    {
        $calendarBackend = new PDO(\Yii::$app->db->getMasterPdo());
        $calendardata = $appointment->davCalendardata;
        $objectUri = Calendarobject::createUid().'.ics';
        
        $calendarBackend->createCalendarObject([$calendarId, 0], $objectUri, $calendardata->serialize());

        $calendardata->destroy();
        
        return Calendarobject::find()
            ->select('id')
            ->andWhere(['{{calendarobjects}}.[[calendarid]]' => $calendarId, '{{calendarobjects}}.[[uri]]' => $objectUri])
            ->scalar();
    }
    
    public static function createEtag(string $calendardata)
    {
        return md5($calendardata);
    }
    
    public static function deleteCalendarObject(int $calendarId, UsersAppointmentsCalendarobjects $appointmentCalendarobject)
    {
        $calendarBackend = new PDO(\Yii::$app->db->getMasterPdo());
        $calendarBackend->deleteCalendarObject([$calendarId, 0], $appointmentCalendarobject->calendarobject->uri);
    }
    
    public static function updateCalendarObject(int $calendarId, UsersAppointmentsCalendarobjects $userAppointmentCalendarobject)
    {
        $calendarBackend = new PDO(\Yii::$app->db->getMasterPdo());
        $calendardata = $userAppointmentCalendarobject->appointment->davCalendardata;
        
        $calendarBackend->updateCalendarObject([$calendarId, 0], $userAppointmentCalendarobject->calendarobject->uri, $calendardata->serialize());
        
        $calendardata->destroy();
        
    }
}

