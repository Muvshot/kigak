<?php

namespace common\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string $category
 * @property string $description
 * @property string $html_color_code 
 *
 * @property Appointment[] $appointments
 * @property User[] $users
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category'], 'required'],
            [['category'], 'string', 'max' => 256],
            [['description'], 'string', 'max' => 255],
            [['html_color_code'], 'string', 'max' => 7],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('label', 'ID'),
            'category' => Yii::t('label', 'Category'),
            'description' => Yii::t('label', 'Description'),
            'html_color_code' => Yii::t('label', 'Html Color Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppointments()
    {
        return $this->hasMany(Appointment::class, ['categories_id' => 'id']);
    }
    
    public function getTag()
    {
        return Html::tag('span', '&nbsp;', ['style' => 'background-color:'.$this->html_color_code, 'class' => 'badge']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::class, ['id' => 'users_id'])->viaTable('categories_users', ['categories_id' => 'id']);
    }
    
    public function __toString()
    {
        return $this->category;
    }
}
