<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "calendars".
 *
 * @property int $id
 * @property int $synctoken
 * @property resource $components
 *
 * @property User[] $users
 */
class Calendar extends \yii\db\ActiveRecord
{
    const COMPONENT_TYPE_EVENT = 'VEVENT';
    const COMPONENT_TYPE_TODO = 'VTODO';
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calendars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['synctoken'], 'integer'],
            [['components'], 'string', 'max' => 21],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('label', 'ID'),
            'synctoken' => Yii::t('label', 'Synctoken'),
            'components' => Yii::t('label', 'Components'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::class, ['id' => 'users_id'])->viaTable('users_calendars', ['calendars_id' => 'id']);
    }
}
