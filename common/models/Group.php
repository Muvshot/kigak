<?php

namespace common\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "groups".
 *
 * @property int $id
 * @property string $group
 * @property string $description
 * @property string $html_color_code
 *
 * @property Appointment[] $appointments
 * @property User[] $users
 * @property array $htmlOptions
 * @property string $tag
 */
class Group extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'groups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group'], 'required'],
            [['group'], 'string', 'max' => 256],
            [['description'], 'string', 'max' => 255],
            [['html_color_code'], 'string', 'max' => 7],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('label', 'ID'),
            'group' => Yii::t('label', 'Group'),
            'description' => Yii::t('label', 'Description'),
            'html_color_code' => Yii::t('label', 'Html Color Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppointments()
    {
        return $this->hasMany(Appointment::class, ['id' => 'appointments_id'])->viaTable('appointments_groups', ['groups_id' => 'id']);
    }
    
    public function getHtmlOptions()
    {
        return ['style' => 'background-color:'.$this->html_color_code, 'class' => 'badge'];
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return Html::tag('span', '&nbsp;', $this->htmlOptions);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::class, ['id' => 'users_id'])->viaTable('groups_users', ['groups_id' => 'id']);
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->tag;
    }
}
