<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "calendarchanges".
 *
 * @property int $id
 * @property resource $uri
 * @property int $synctoken
 * @property int $calendarid
 * @property int $operation
 */
class Calendarchanges extends \yii\db\ActiveRecord
{
    const OPERATION_CREATE = 1;
    const OPERATION_UPDATE = 2;
    const OPERATION_DELETE = 3;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calendarchanges';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uri', 'synctoken', 'calendarid', 'operation'], 'required'],
            [['synctoken', 'calendarid', 'operation'], 'integer'],
            [['uri'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('label', 'ID'),
            'uri' => Yii::t('label', 'Uri'),
            'synctoken' => Yii::t('label', 'Synctoken'),
            'calendarid' => Yii::t('label', 'Calendarid'),
            'operation' => Yii::t('label', 'Operation'),
        ];
    }
}
