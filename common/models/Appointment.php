<?php

namespace common\models;

use DateInterval;
use DateTime;
use Exception;
use Sabre\VObject;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "appointments".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $beginDate
 * @property string $endDate
 * @property string $beginTime
 * @property string $endTime
 * @property string $repeatInterval
 * @property int $categories_id
 *
 * @property Category $category
 * @property VObject\Component\VCalendar $davCalendardata
 * @property DateTime $end
 * @property Group[] $groups
 * @property DateTime $start
 * @property UsersAppointmentsCalendarobjects[] $usersAppointmentsCalendarobjects
 *
 */
class Appointment extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('label', 'ID'),
            'title' => Yii::t('label', 'Title'),
            'description' => Yii::t('label', 'Description'),
            'beginDate' => Yii::t('label', 'Begin Date'),
            'endDate' => Yii::t('label', 'End Date'),
            'beginTime' => Yii::t('label', 'Begin Time'),
            'endTime' => Yii::t('label', 'End Time'),
            'repeatInterval' => Yii::t('label', 'Repeat Interval'),
            'categories_id' => Yii::t('label', 'Category'),
        ];
    }

    public function getDavCalendardata()
    {
        $start = $this->start;
        $end = $this->end;

        if($this->hasTime()) {
            return new VObject\Component\VCalendar([
                'VEVENT' => [
                    'SUMMARY' => $this->title,
                    'DESCRIPTION' => $this->description,
                    'DTSTART' => $start,
                    'DTEND' => $end,
                ],
            ]);
        } else {
            /**
             * If Time is not defined use Date as Format
             * Important: In this Case the End-Date is not inclusive, so we add 1 Day
             *
             * See:
             * http://tools.ietf.org/html/rfc5545#page-54
             */
            return new VObject\Component\VCalendar([
                'VEVENT' => [
                    'SUMMARY' => $this->title,
                    'DESCRIPTION' => $this->description,
                    'DTSTART' => $start->format('Ymd'),
                    'DTEND' => $end->add(new DateInterval('P1D'))->format('Ymd'),
                ],
            ]);
        }
    }

    protected function hasTime()
    {
        return !empty($this->beginTime);
    }

    /**
     * @return ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'categories_id']);
    }

    /**
     * @return DateTime
     * @throws Exception
     */
    public function getEnd()
    {
        if(!empty($this->endTime)) {
            return new DateTime($this->endDate . ' ' . $this->endTime);
        } else {
            return new DateTime($this->endDate);
        }
    }

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getGroups()
    {
        return $this->hasMany(Group::class, ['id' => 'groups_id'])->viaTable('appointments_groups', ['appointments_id' => 'id'])->indexBy('id');
    }

    /**
     * @return DateTime
     * @throws Exception
     */
    public function getStart()
    {
        if(!empty($this->beginTime)) {
            return new DateTime($this->beginDate . ' ' . $this->beginTime);
        } else {
            return new DateTime($this->beginDate);
        }
    }
    
    /**
     * @return ActiveQuery
     */
    public function getUsersAppointmentsCalendarobjects()
    {
        return $this->hasMany(UsersAppointmentsCalendarobjects::class, ['appointments_id' => 'id']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'beginDate', 'endDate', 'categories_id'], 'required'],
            [['description'], 'string'],
            [['beginDate', 'endDate', 'beginTime', 'endTime'], 'safe'],
            [['categories_id'], 'integer'],
            [['title', 'repeatInterval'], 'string', 'max' => 255],
            [['categories_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['categories_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     * @return query\AppointmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new query\AppointmentQuery(get_called_class());
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'appointments';
    }
}
