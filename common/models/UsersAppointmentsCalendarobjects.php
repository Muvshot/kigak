<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "users_appointments_calendarobjects".
 *
 * @property int $users_id
 * @property int $appointments_id
 * @property int $calendarobjects_id
 *
 * @property Appointment $appointment
 * @property Calendarobject $calendarobject
 * @property User $user
 */
class UsersAppointmentsCalendarobjects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_appointments_calendarobjects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['users_id', 'appointments_id', 'calendarobjects_id'], 'required'],
            [['users_id', 'appointments_id', 'calendarobjects_id'], 'integer'],
            [['users_id', 'appointments_id', 'calendarobjects_id'], 'unique', 'targetAttribute' => ['users_id', 'appointments_id', 'calendarobjects_id']],
            [['appointments_id'], 'exist', 'skipOnError' => true, 'targetClass' => Appointment::class, 'targetAttribute' => ['appointments_id' => 'id']],
            [['calendarobjects_id'], 'exist', 'skipOnError' => true, 'targetClass' => Calendarobject::class, 'targetAttribute' => ['calendarobjects_id' => 'id']],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['users_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'users_id' => Yii::t('label', 'User ID'),
            'appointments_id' => Yii::t('label', 'Appointments ID'),
            'calendarobjects_id' => Yii::t('label', 'Calendarobjects ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppointment()
    {
        return $this->hasOne(Appointment::class, ['id' => 'appointments_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCalendarobject()
    {
        return $this->hasOne(Calendarobject::class, ['id' => 'calendarobjects_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'users_id']);
    }
}
