<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "calendarinstances".
 *
 * @property int $id
 * @property int $calendarid
 * @property resource $principaluri
 * @property int $access 1 = owner, 2 = read, 3 = readwrite
 * @property string $displayname
 * @property resource $uri
 * @property string $description
 * @property int $calendarorder
 * @property resource $calendarcolor
 * @property string $timezone
 * @property int $transparent
 * @property resource $share_href
 * @property string $share_displayname
 * @property int $share_invitestatus 1 = noresponse, 2 = accepted, 3 = declined, 4 = invalid
 */
class Calendarinstance extends \yii\db\ActiveRecord
{
    const MAIN_DISPLAY_NAME = 'Johannes Kindergarten';
    const DEFAULT_ACCESS = 1;
    const DEFAULT_COLOR = '#FF2968';
    const DEFAULT_ORDER = 0;
    const DEFAULT_SHARE_STATUS = 2;
    const DEFAULT_TIMEZONE = <<<TIMEZONESTRING
BEGIN:VCALENDAR
VERSION:2.0
CALSCALE:GREGORIAN
BEGIN:VTIMEZONE
TZID:Europe/Berlin
BEGIN:DAYLIGHT
TZOFFSETFROM:+0100
RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU
DTSTART:19810329T020000
TZNAME:MESZ
TZOFFSETTO:+0200
END:DAYLIGHT
BEGIN:STANDARD
TZOFFSETFROM:+0200
RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU
DTSTART:19961027T030000
TZNAME:MEZ
TZOFFSETTO:+0100
END:STANDARD
END:VTIMEZONE
END:VCALENDAR
TIMEZONESTRING;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calendarinstances';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['calendarid'], 'required'],
            [['calendarid', 'access', 'calendarorder', 'transparent', 'share_invitestatus'], 'integer'],
            [['description', 'timezone'], 'string'],
            [['principaluri', 'displayname', 'share_href', 'share_displayname'], 'string', 'max' => 100],
            [['uri'], 'string', 'max' => 200],
            [['calendarcolor'], 'string', 'max' => 10],
            [['principaluri', 'uri'], 'unique', 'targetAttribute' => ['principaluri', 'uri']],
            [['calendarid', 'principaluri'], 'unique', 'targetAttribute' => ['calendarid', 'principaluri']],
            [['calendarid', 'share_href'], 'unique', 'targetAttribute' => ['calendarid', 'share_href']],
            [['access'], 'default', 'value' => self::DEFAULT_ACCESS ],
            [['calendarcolor'], 'default', 'value' => self::DEFAULT_COLOR ],
            [['calendarorder'], 'default', 'value' => self::DEFAULT_ORDER ],
            [['share_invitestatus'], 'default', 'value' => self::DEFAULT_SHARE_STATUS ],
            [['timezone'], 'default', 'value' => self::DEFAULT_TIMEZONE],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('label', 'ID'),
            'calendarid' => Yii::t('label', 'Calendarid'),
            'principaluri' => Yii::t('label', 'Principaluri'),
            'access' => Yii::t('label', 'Access'),
            'displayname' => Yii::t('label', 'Displayname'),
            'uri' => Yii::t('label', 'Uri'),
            'description' => Yii::t('label', 'Description'),
            'calendarorder' => Yii::t('label', 'Calendarorder'),
            'calendarcolor' => Yii::t('label', 'Calendarcolor'),
            'timezone' => Yii::t('label', 'Timezone'),
            'transparent' => Yii::t('label', 'Transparent'),
            'share_href' => Yii::t('label', 'Share Href'),
            'share_displayname' => Yii::t('label', 'Share Displayname'),
            'share_invitestatus' => Yii::t('label', 'Share Invitestatus'),
        ];
    }
    
    public static function createUid()
    {
        $uid = strtoupper(md5(microtime(true)));
        $uid = substr_replace($uid,'-', 8,0);
        $uid = substr_replace($uid,'-', 13,0);
        $uid = substr_replace($uid,'-', 18,0);
        $uid = substr_replace($uid,'-', 23,0);
        return $uid;
    }
}
