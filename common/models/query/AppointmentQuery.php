<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\Appointment]].
 *
 * @see \common\models\Appointment
 */
class AppointmentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\models\Appointment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
    
    public function byCategory($ids)
    {
        return $this->andWhere(['{{appointments}}.[[categories_id]]' => $ids]);
    }
    
    public function byGroup($ids)
    {
        return $this->innerJoinWith('groups', false)
            ->andWhere(['{{groups}}.[[id]]'=> $ids]);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Appointment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
