<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\User]].
 *
 * @see \common\models\User
 */
class UserQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[status]]>1');
    }

    /**
     * {@inheritdoc}
     * @return \common\models\User[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param int $categoryId
     * @return UserQuery
     */
    public function byCategory(int $categoryId)
    {
        return $this->innerJoinWith('categories', false)
            ->andWhere(['{{categories}}.[[id]]' => $categoryId]);
    }

    /**
     * @param array $groupIds
     * @return UserQuery
     */
    public function byGroups(array $groupIds)
    {
        return $this->innerJoinWith('groups', false)
            ->andWhere(['{{groups}}.[[id]]' => $groupIds]);
    }

    /**
     * @param $username
     * @return UserQuery
     */
    public function byUsername($username)
    {
        return $this->andWhere(['[[username]]' => $username]);
    }

    /**
     * @param $status
     * @return UserQuery
     */
    public function minStatus($status)
    {
        return $this->andWhere(['>=', '[[status]]', $status]);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\User|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
