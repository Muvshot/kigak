<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "principals".
 *
 * @property int $id
 * @property resource $uri
 * @property resource $email
 * @property string $displayname
 */
class Principal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'principals';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uri'], 'required'],
            [['uri'], 'string', 'max' => 200],
            [['email', 'displayname'], 'string', 'max' => 80],
            [['uri'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('label', 'ID'),
            'uri' => Yii::t('label', 'Uri'),
            'email' => Yii::t('label', 'Email'),
            'displayname' => Yii::t('label', 'Displayname'),
        ];
    }
}
