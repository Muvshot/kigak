<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 *
 * @property integer calendarId
 * @property Calendar[] $calendars
 * @property Category[] $categories
 * @property Group[] $groups
 * @property Calendarinstance $mainCalendarinstance
 * @property UsersAppointmentsCalendarobjects[] $usersAppointmentsCalendarobjects
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_SIGNEDUP = 1;
    const STATUS_ACTIVE = 5;
    const STATUS_SYSADMIN = 10;

    const PRINCIPAL_URI = 'principals';
    const CALENDAR_URI = 'calendars';

    /**
     * @throws Exception
     */
    public function init()
    {
        parent::init();
        $this->status = self::STATUS_SIGNEDUP;
        $this->generateAuthKey();
        $this->password_hash = Yii::$app->security->generatePasswordHash(Yii::$app->security->generateRandomString());
    }

    public function addUsersAppointmentsCalendarobject($toCreateId, $calendarobjectId)
    {
        (new UsersAppointmentsCalendarobjects([
            'users_id' => $this->id,
            'appointments_id' => $toCreateId,
            'calendarobjects_id' => $calendarobjectId
        ]))->save();
    }

    /**
     * @param int $appointmentId
     * @return UsersAppointmentsCalendarobjects|ActiveRecord|null
     */
    public function getAppointmentCalendarObject(int $appointmentId)
    {
        return $this->getUsersAppointmentsCalendarobjects()->andWhere(['{{users_appointments_calendarobjects}}.[[appointments_id]]' => $appointmentId])->one();
    }

    public function getCalendarId()
    {
        return $this->mainCalendarinstance->calendarid;
    }

    /**
     * @return ActiveQuery
     */
    public function getCalendarinstances()
    {
        return $this->hasMany(Calendarinstance::class, ['calendarid' => 'id'])->via('calendars');
    }

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getCalendars()
    {
        return $this->hasMany(Calendar::class, ['id' => 'calendars_id'])->viaTable('users_calendars', ['users_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getCategories()
    {
        return $this->hasMany(Category::class, ['id' => 'categories_id'])->viaTable('categories_users', ['users_id' => 'id'])->indexBy('id');
    }

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getGroups()
    {
        return $this->hasMany(Group::class, ['id' => 'groups_id'])->viaTable('groups_users', ['users_id' => 'id'])->indexBy('id');
    }

    /**
     * @return array|Calendarinstance|ActiveRecord
     */
    public function getMainCalendarinstance()
    {
        return $this->getCalendarinstances()->andWhere('{{calendarinstances}}.[[calendarorder]]=0')->one();
    }

    public function getRootCalendarsPath()
    {
        return self::CALENDAR_URI.'/'.$this->username;
    }

    public function getRootPrincipalPath()
    {
        return self::PRINCIPAL_URI.'/'.$this->username;
    }

    /**
     * @return ActiveQuery
     */
    public function getUsersAppointmentsCalendarobjects()
    {
        return $this->hasMany(UsersAppointmentsCalendarobjects::class, ['users_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email'], 'required'],
            [['status'], 'in', 'range' => [self::STATUS_DELETED, self::STATUS_SIGNEDUP, self::STATUS_ACTIVE, self::STATUS_SYSADMIN]],
            [['username'], 'unique'],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     * @return query\UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new query\UserQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, ['>=', 'status', self::STATUS_ACTIVE]]);
    }

    /**
     * {@inheritdoc}
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::find()->where(['and',
            ['password_reset_token' => $token],
            ['>=', 'status', self::STATUS_ACTIVE],
        ])->one();
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws Exception
     */
    public function setPassword(string $password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     * @throws Exception
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     * @throws Exception
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}