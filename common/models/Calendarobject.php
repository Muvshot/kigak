<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "calendarobjects".
 *
 * @property int $id
 * @property string $calendardata
 * @property resource $uri
 * @property int $calendarid
 * @property int $lastmodified
 * @property resource $etag
 * @property int $size
 * @property resource $componenttype
 * @property int $firstoccurence
 * @property int $lastoccurence
 * @property resource $uid
 *
 * @property UsersAppointmentsCalendarobjects[] $usersAppointmentsCalendarobjects
 */
class Calendarobject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calendarobjects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['calendardata'], 'string'],
            [['calendarid', 'size'], 'required'],
            [['calendarid', 'lastmodified', 'size', 'firstoccurence', 'lastoccurence'], 'integer'],
            [['uri', 'uid'], 'string', 'max' => 200],
            [['etag'], 'string', 'max' => 32],
            [['componenttype'], 'string', 'max' => 8],
            [['calendarid', 'uri'], 'unique', 'targetAttribute' => ['calendarid', 'uri']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('label', 'ID'),
            'calendardata' => Yii::t('label', 'Calendardata'),
            'uri' => Yii::t('label', 'Uri'),
            'calendarid' => Yii::t('label', 'Calendarid'),
            'lastmodified' => Yii::t('label', 'Lastmodified'),
            'etag' => Yii::t('label', 'Etag'),
            'size' => Yii::t('label', 'Size'),
            'componenttype' => Yii::t('label', 'Componenttype'),
            'firstoccurence' => Yii::t('label', 'Firstoccurence'),
            'lastoccurence' => Yii::t('label', 'Lastoccurence'),
            'uid' => Yii::t('label', 'Uid'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersAppointmentsCalendarobjects()
    {
        return $this->hasMany(UsersAppointmentsCalendarobjects::class, ['calendarobjects_id' => 'id']);
    }
    
    public static function createUid()
    {
        $uid = strtolower(md5(microtime(true)));
        $uid = substr_replace($uid,'-', 8,0);
        $uid = substr_replace($uid,'-', 13,0);
        $uid = substr_replace($uid,'-', 18,0);
        $uid = substr_replace($uid,'-', 23,0);
        return $uid.'.'.microtime(true);
    }
}
