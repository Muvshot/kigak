<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

if(getenv('APP_ENVIRONMENT')) {
    defined('YII_DEBUG') or define('YII_DEBUG', false);
} else {
    defined('YII_DEBUG') or define('YII_DEBUG', true);
}
defined('YII_ENV') or define('YII_ENV', getenv('YII_ENVIRONMENT'));
