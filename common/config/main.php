<?php

use kartik\datecontrol\Module as DatecontrolModule;

return [
    'name' => 'Kindergarten Kalendar',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            //'dateFormat' => 'php:d-M-Y',
            //'datetimeFormat' => 'php:d-M-Y H:i:s',
            'timeFormat' => 'php:H:i',
        ],
        'i18n' => [
            'translations' => [
                'label*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'backend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                    'fileMap' => [
                        'backend' => 'backend.php',
                        'backend/error' => 'error.php',
                    ],
                ],
                'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'frontend' => 'frontend.php',
                        'frontend/error' => 'error.php',
                    ],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            /*'rules' => [
                '/' => '/appointment/index',
            ],*/
        ],
    ],
    'language' => 'de-DE',
    'modules' => [
        'datecontrol' =>  [
            'class' => '\kartik\datecontrol\Module',
            'ajaxConversion' => true,
            'autoWidgetSettings' => [
                DatecontrolModule::FORMAT_DATE => ['pluginOptions'=>['autoclose'=>true]], // example
                DatecontrolModule::FORMAT_DATETIME => ['pluginOptions'=>['autoclose'=>true]], // setup if needed
                DatecontrolModule::FORMAT_TIME => ['pluginOptions'=>['autoclose'=>true]], // setup if needed
            ],
            'displaySettings' => [
                DatecontrolModule::FORMAT_DATE => 'dd.MM.yyyy',
                DatecontrolModule::FORMAT_TIME => 'HH:mm',
                DatecontrolModule::FORMAT_DATETIME => 'dd-MM-yyyy HH:mm:ss',
            ],
            'saveSettings' => [
                DatecontrolModule::FORMAT_DATE => 'php:Y-m-d',
                DatecontrolModule::FORMAT_TIME => 'php:H:i:s',
                DatecontrolModule::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
            ],
        ]
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
];
