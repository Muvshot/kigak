<?php
return [
    'adminEmail' => 'admin@kigak.de',
    'supportEmail' => 'support@kigak.de',
    'user.passwordResetTokenExpire' => 3600,
    'backendUrl' => 'http://backend.'.getenv('DOMAINNAME'),
    'frontendUrl' => 'http://'.getenv('DOMAINNAME'),
];
