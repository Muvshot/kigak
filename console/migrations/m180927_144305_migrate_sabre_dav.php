<?php

use yii\db\Migration;

/**
 * Class m180927_144305_migrate_sabre_dav
 */
class m180927_144305_migrate_sabre_dav extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $sql = file_get_contents('console/migrations/sabre/mysql.addressbooks.sql');
        $this->execute($sql);
        $sql = file_get_contents('console/migrations/sabre/mysql.calendars.sql');
        $this->execute($sql);
        $sql = file_get_contents('console/migrations/sabre/mysql.locks.sql');
        $this->execute($sql);
        $sql = file_get_contents('console/migrations/sabre/mysql.principals.sql');
        $this->execute($sql);
        $sql = file_get_contents('console/migrations/sabre/mysql.propertystorage.sql');
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('propertystorage');
        $this->dropTable('groupmembers');
        $this->dropTable('principals');
        $this->dropTable('locks');
        $this->dropTable('schedulingobjects');
        $this->dropTable('calendarsubscriptions');
        $this->dropTable('calendarchanges');
        $this->dropTable('calendarinstances');
        $this->dropTable('calendarobjects');
        $this->dropTable('calendars');
        $this->dropTable('addressbookchanges');
        $this->dropTable('cards');
        $this->dropTable('addressbooks');
    }

}
