<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_appointments_calendarobjects`.
 * Has foreign keys to the tables:
 *
 * - `appointments`
 * - `users`
 * - `calendarobjects`
 */
class m180930_141801_create_junction_table_for_users_and_appointments_and_calendarobjects_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users_appointments_calendarobjects', [
            'users_id' => $this->integer(),
            'appointments_id' => $this->integer(),
            'calendarobjects_id' => $this->integer()->unsigned(),
            'PRIMARY KEY(users_id, appointments_id, calendarobjects_id)',
        ]);
        
        // creates index for column `users_id`
        $this->createIndex(
            'idx-users_appointments_calendarobjects-users_id',
            'users_appointments_calendarobjects',
            'users_id'
            );
        
        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-users_appointments_calendarobjects-users_id',
            'users_appointments_calendarobjects',
            'users_id',
            'users',
            'id',
            'CASCADE'
            );

        // creates index for column `appointments_id`
        $this->createIndex(
            'idx-users_appointments_calendarobjects-appointments_id',
            'users_appointments_calendarobjects',
            'appointments_id'
        );

        // add foreign key for table `appointments`
        $this->addForeignKey(
            'fk-users_appointments_calendarobjects-appointments_id',
            'users_appointments_calendarobjects',
            'appointments_id',
            'appointments',
            'id',
            'CASCADE'
        );

        // creates index for column `calendarobjects_id`
        $this->createIndex(
            'idx-users_appointments_calendarobjects-calendarobjects_id',
            'users_appointments_calendarobjects',
            'calendarobjects_id'
        );

        // add foreign key for table `calendarobjects`
        $this->addForeignKey(
            'fk-users_appointments_calendarobjects-calendarobjects_id',
            'users_appointments_calendarobjects',
            'calendarobjects_id',
            'calendarobjects',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `users`
        $this->dropForeignKey(
            'fk-users_appointments_calendarobjects-users_id',
            'users_appointments_calendarobjects'
        );
        
        // drops index for column `users_id`
        $this->dropIndex(
            'idx-users_appointments_calendarobjects-users_id',
            'users_appointments_calendarobjects'
        );
        
        // drops foreign key for table `appointments`
        $this->dropForeignKey(
            'fk-users_appointments_calendarobjects-appointments_id',
            'users_appointments_calendarobjects'
        );

        // drops index for column `appointments_id`
        $this->dropIndex(
            'idx-users_appointments_calendarobjects-appointments_id',
            'users_appointments_calendarobjects'
        );

        // drops foreign key for table `calendarobjects`
        $this->dropForeignKey(
            'fk-users_appointments_calendarobjects-calendarobjects_id',
            'users_appointments_calendarobjects'
        );

        // drops index for column `calendarobjects_id`
        $this->dropIndex(
            'idx-users_appointments_calendarobjects-calendarobjects_id',
            'users_appointments_calendarobjects'
        );

        $this->dropTable('users_appointments_calendarobjects');
    }
}
