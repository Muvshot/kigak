<?php

use yii\db\Migration;

/**
 * Handles the creation of table `groups`.
 */
class m180929_104836_create_groups_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('groups', [
            'id' => $this->primaryKey(),
            'group' => $this->string(256)->notNull(),
            'description' => $this->string(),
            'html_color_code' => $this->string(7),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('groups');
    }
}
