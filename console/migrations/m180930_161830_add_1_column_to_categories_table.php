<?php

use yii\db\Migration;

/**
 * Handles adding 1 to table `categories`.
 */
class m180930_161830_add_1_column_to_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('categories', 'html_color_code', $this->string(7));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('categories', 'html_color_code');
    }
}
