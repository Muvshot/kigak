<?php

use yii\db\Migration;

/**
 * Handles the creation of table `groups_users`.
 * Has foreign keys to the tables:
 *
 * - `groups`
 * - `users`
 */
class m180930_141708_create_junction_table_for_groups_and_users_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('groups_users', [
            'groups_id' => $this->integer(),
            'users_id' => $this->integer(),
            'PRIMARY KEY(groups_id, users_id)',
        ]);

        // creates index for column `groups_id`
        $this->createIndex(
            'idx-groups_users-groups_id',
            'groups_users',
            'groups_id'
        );

        // add foreign key for table `groups`
        $this->addForeignKey(
            'fk-groups_users-groups_id',
            'groups_users',
            'groups_id',
            'groups',
            'id',
            'CASCADE'
        );

        // creates index for column `users_id`
        $this->createIndex(
            'idx-groups_users-users_id',
            'groups_users',
            'users_id'
        );

        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-groups_users-users_id',
            'groups_users',
            'users_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `groups`
        $this->dropForeignKey(
            'fk-groups_users-groups_id',
            'groups_users'
        );

        // drops index for column `groups_id`
        $this->dropIndex(
            'idx-groups_users-groups_id',
            'groups_users'
        );

        // drops foreign key for table `users`
        $this->dropForeignKey(
            'fk-groups_users-users_id',
            'groups_users'
        );

        // drops index for column `users_id`
        $this->dropIndex(
            'idx-groups_users-users_id',
            'groups_users'
        );

        $this->dropTable('groups_users');
    }
}
