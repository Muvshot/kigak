<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_calendars`.
 * Has foreign keys to the tables:
 *
 * - `users`
 * - `calendars`
 */
class m181005_090921_create_junction_table_for_users_and_calendars_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users_calendars', [
            'users_id' => $this->integer(),
            'calendars_id' => $this->integer()->unsigned(),
            'PRIMARY KEY(users_id, calendars_id)',
        ]);

        // creates index for column `users_id`
        $this->createIndex(
            'idx-users_calendars-users_id',
            'users_calendars',
            'users_id'
        );

        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-users_calendars-users_id',
            'users_calendars',
            'users_id',
            'users',
            'id',
            'CASCADE'
        );

        // creates index for column `calendars_id`
        $this->createIndex(
            'idx-users_calendars-calendars_id',
            'users_calendars',
            'calendars_id'
        );

        // add foreign key for table `calendars`
        $this->addForeignKey(
            'fk-users_calendars-calendars_id',
            'users_calendars',
            'calendars_id',
            'calendars',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `users`
        $this->dropForeignKey(
            'fk-users_calendars-users_id',
            'users_calendars'
        );

        // drops index for column `users_id`
        $this->dropIndex(
            'idx-users_calendars-users_id',
            'users_calendars'
        );

        // drops foreign key for table `calendars`
        $this->dropForeignKey(
            'fk-users_calendars-calendars_id',
            'users_calendars'
        );

        // drops index for column `calendars_id`
        $this->dropIndex(
            'idx-users_calendars-calendars_id',
            'users_calendars'
        );

        $this->dropTable('users_calendars');
    }
}
