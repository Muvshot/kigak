<?php

use yii\db\Migration;

/**
 * Handles the creation of table `appointment`.
 */
class m180929_104900_create_appointments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%appointments}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'description' => $this->text(),
            'beginDate' => $this->date()->notNull(),
            'endDate' => $this->date()->notNull(),
            'beginTime' => $this->time()->defaultValue(Null),
            'endTime' => $this->time()->defaultValue(Null),
            'repeatInterval' => $this->string()->defaultValue(NULL),
            'categories_id' => $this->integer()->notNull()
        ]);
        
        // creates index for column `categories_id`
        $this->createIndex(
            'idx-appointments-categories_id',
            'appointments',
            'categories_id'
        );
        
        // add foreign key for table `categories`
        $this->addForeignKey(
            'fk-appointments-categories_id',
            'appointments',
            'categories_id',
            'categories',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `categories`
        $this->dropForeignKey(
            'fk-appointments-categories_id',
            'appointments'
        );
        
        // drops index for column `categories_id`
        $this->dropIndex(
            'idx-appointments-categories_id',
            'appointments'
        );
        
        
        $this->dropTable('{{%appointments}}');
    }
}
