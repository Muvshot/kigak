<?php

use yii\db\Migration;

/**
 * Handles the creation of table `appointments_groups`.
 * Has foreign keys to the tables:
 *
 * - `appointments`
 * - `groups`
 */
class m180929_104932_create_junction_table_for_appointments_and_groups_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('appointments_groups', [
            'appointments_id' => $this->integer(),
            'groups_id' => $this->integer(),
            'PRIMARY KEY(appointments_id, groups_id)',
        ]);

        // creates index for column `appointments_id`
        $this->createIndex(
            'idx-appointments_groups-appointments_id',
            'appointments_groups',
            'appointments_id'
        );

        // add foreign key for table `appointments`
        $this->addForeignKey(
            'fk-appointments_groups-appointments_id',
            'appointments_groups',
            'appointments_id',
            'appointments',
            'id',
            'CASCADE'
        );

        // creates index for column `groups_id`
        $this->createIndex(
            'idx-appointments_groups-groups_id',
            'appointments_groups',
            'groups_id'
        );

        // add foreign key for table `groups`
        $this->addForeignKey(
            'fk-appointments_groups-groups_id',
            'appointments_groups',
            'groups_id',
            'groups',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `appointments`
        $this->dropForeignKey(
            'fk-appointments_groups-appointments_id',
            'appointments_groups'
        );

        // drops index for column `appointments_id`
        $this->dropIndex(
            'idx-appointments_groups-appointments_id',
            'appointments_groups'
        );

        // drops foreign key for table `groups`
        $this->dropForeignKey(
            'fk-appointments_groups-groups_id',
            'appointments_groups'
        );

        // drops index for column `groups_id`
        $this->dropIndex(
            'idx-appointments_groups-groups_id',
            'appointments_groups'
        );

        $this->dropTable('appointments_groups');
    }
}
