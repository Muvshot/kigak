<?php

use yii\db\Migration;

/**
 * Handles the creation of table `categories_users`.
 * Has foreign keys to the tables:
 *
 * - `categories`
 * - `users`
 */
class m180930_141658_create_junction_table_for_categories_and_users_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('categories_users', [
            'categories_id' => $this->integer(),
            'users_id' => $this->integer(),
            'PRIMARY KEY(categories_id, users_id)',
        ]);

        // creates index for column `categories_id`
        $this->createIndex(
            'idx-categories_users-categories_id',
            'categories_users',
            'categories_id'
        );

        // add foreign key for table `categories`
        $this->addForeignKey(
            'fk-categories_users-categories_id',
            'categories_users',
            'categories_id',
            'categories',
            'id',
            'CASCADE'
        );

        // creates index for column `users_id`
        $this->createIndex(
            'idx-categories_users-users_id',
            'categories_users',
            'users_id'
        );

        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-categories_users-users_id',
            'categories_users',
            'users_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `categories`
        $this->dropForeignKey(
            'fk-categories_users-categories_id',
            'categories_users'
        );

        // drops index for column `categories_id`
        $this->dropIndex(
            'idx-categories_users-categories_id',
            'categories_users'
        );

        // drops foreign key for table `users`
        $this->dropForeignKey(
            'fk-categories_users-users_id',
            'categories_users'
        );

        // drops index for column `users_id`
        $this->dropIndex(
            'idx-categories_users-users_id',
            'categories_users'
        );

        $this->dropTable('categories_users');
    }
}
